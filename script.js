// Теоретичне питання:
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Асинхронність у JS означає, що операції можуть виконуватися паралельно, тобто одночасно, без очікування результатів попередніх операцій.

const findBtn = document.getElementById("find");
const result = document.getElementById("result");

async function getIP(ip) {
    const response = await fetch(ip)
    const result = await response.json()
    return result
}

async function findIp() {
    const ipResult = await getIP('http://api.ipify.org/?format=json')
    const ip = ipResult.ip
    
    const locationResult = await getIP(`http://ip-api.com/json/${ip}`)
    const continent = locationResult.timezone.split('/')[0]
    const country = locationResult.country
    const region = locationResult.region
    const city = locationResult.city
    const district = locationResult.regionName

    result.innerHTML = `
    <p>Континет: ${continent} </p> 
    <p>Країна: ${country} </p> 
    <p>Регіон: ${region} </p> 
    <p>Місто: ${city} </p> 
    <p>Район: ${district} </p>
    `;
}

findBtn.addEventListener('click', () => {
    findIp()
})

